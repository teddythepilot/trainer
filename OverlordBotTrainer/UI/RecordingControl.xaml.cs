﻿using NAudio.Utils;
using NAudio.Wave;
using OverlordBotTrainer.Audio;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Controls;
using NAudio.CoreAudioApi;

namespace OverlordBotTrainer.UI
{
    public partial class RecordingControl
    {
        private static readonly WaveFormat WaveFormat = new WaveFormat(16000, 16, 1);

        private AudioInput AudioInput { get; } = AudioInput.Instance;
        private AudioOutput AudioOutput { get; } = AudioOutput.Instance;

        private bool _recording;
        public bool RecordingAccepted { get; private set; }

        private readonly List<string> _airfieldNames;

        private WaveIn _sourceStream;
        private WaveFileWriter _waveFileWriter;
        private Stream _memoryStream;

        public RawSourceWaveStream RecordedAudio => new RawSourceWaveStream(_memoryStream, WaveFormat);

        public RecordingControl(Dictionary<string, List<string>> airfields, string text)
        {
            InitializeComponent();
            SampleSentence.Text = text;
            AirfieldName.Visibility = System.Windows.Visibility.Hidden;
            if (airfields == null) return;

            foreach (var key in airfields.Keys.Where(text.Contains))
            {
                _airfieldNames = airfields[key];
            }

            if (_airfieldNames.Count == 1) return;

            AirfieldName.ItemsSource = _airfieldNames;
            AirfieldName.SelectedIndex = 0;
            AirfieldName.Visibility = System.Windows.Visibility.Visible;
        }

        private void ToggleRecording(object sender, System.Windows.RoutedEventArgs e)
        {
            _recording = !_recording;

            if(_recording)
            {
                RecordingButton.Content = "Stop";
                StartRecording();
            }
            else
            {
                RecordingButton.Content = "Record";
                StopRecording();
            }
        }

        private void StartRecording()
        {

            _sourceStream = new WaveIn
            {
                DeviceNumber = AudioInput.SelectedAudioInputDeviceNumber(),
                WaveFormat = WaveFormat
            };

            _sourceStream.DataAvailable += RecordingDataAvailable;

            _memoryStream = new MemoryStream();
            _waveFileWriter = new WaveFileWriter(new IgnoreDisposeStream(_memoryStream), _sourceStream.WaveFormat);

            _sourceStream.StartRecording();
            PlaybackButton.IsEnabled = false;
            RecordingAcceptedCheckbox.IsEnabled = false;
        }

        private void StopRecording()
        {
            _sourceStream.StopRecording();
            _sourceStream.Dispose();
            _waveFileWriter.Flush();
            _waveFileWriter.Dispose();
            PlaybackButton.IsEnabled = true;
        }

        private void RecordingDataAvailable(object sender, WaveInEventArgs e)
        {
            _waveFileWriter.Write(e.Buffer, 0, e.BytesRecorded);
        }

        private void PlayRecording(object sender, System.Windows.RoutedEventArgs e)
        {
            if (_memoryStream == null) return;
            RecordingButton.IsEnabled = false;
            RecordingAcceptedCheckbox.IsEnabled = false;
            AirfieldName.IsEnabled = false;
            var reader = new RawSourceWaveStream(_memoryStream, WaveFormat)
            {
                Position = 0
            };

            var waveOut = new WasapiOut((MMDevice)AudioOutput.SelectedAudioOutput.Value, AudioClientShareMode.Shared, true, 80 );
            waveOut.Init(reader);
            waveOut.PlaybackStopped += AudioOutput_PlaybackStopped;
            waveOut.Play();
        }

        public void AudioOutput_PlaybackStopped(object sender, StoppedEventArgs e)
        {
            RecordingButton.IsEnabled = true;
            RecordingAcceptedCheckbox.IsEnabled = true;
            AirfieldName.IsEnabled = true;
        }

        private void AcceptRecording(object sender, System.Windows.RoutedEventArgs e)
        {
            RecordingAccepted = true;
            RecordingButton.IsEnabled = false;
            PlaybackButton.IsEnabled = false;
            AirfieldName.IsEnabled = false;
        }

        private void RejectRecording(object sender, System.Windows.RoutedEventArgs e)
        {
            RecordingAccepted = false;
            RecordingButton.IsEnabled = true;
            PlaybackButton.IsEnabled = true;
            AirfieldName.IsEnabled = true;
        }

        private void AirfieldName_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var oldValue = (sender as ComboBox)?.Text;
            var newValue = (sender as ComboBox)?.SelectedItem as string;

            if (oldValue == null || oldValue.Equals("")) return;
            SampleSentence.Text = SampleSentence.Text.Replace(oldValue, newValue);
            RecordingAccepted = false;
            RecordingButton.IsEnabled = true;
            PlaybackButton.IsEnabled = false;
            RecordingAcceptedCheckbox.IsEnabled = false;
        }
    }
}
