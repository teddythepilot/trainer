﻿using OverlordBotTrainer.Audio;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Navigation;

namespace OverlordBotTrainer.UI
{
    public partial class RecordingWindow
    {
        private static readonly Random Randomizer = new Random();

        private readonly string _playerCallsign;

        public AudioInput AudioInput { get; } = AudioInput.Instance;

        public AudioOutput AudioOutput { get; } = AudioOutput.Instance;

        public RecordingWindow(string PlayerCallsign)
        {
            InitializeComponent();
            DataContext = this;

            _playerCallsign = PlayerCallsign;

            PopulateAwacsSamples();
            PopulateAtcSamples();
        }

        private void PopulateAwacsSamples()
        {

            var awacsSamples = new ArrayList();

            foreach (string call in Data.SetWarningRadiusCalls)
            {
                awacsSamples.Add($"{Random(Data.AwacsCallsigns)} {_playerCallsign} {call} {Random(Data.SetWarningRadiusMiles)} miles");
            }

            
            foreach (string call in Data.BogeyDopeCalls)
            {
                awacsSamples.Add($"{Random(Data.AwacsCallsigns)} {_playerCallsign} {call}");
            }

            
            foreach (string call in Data.RadioCheckCalls)
            {
                awacsSamples.Add($"{Random(Data.AwacsCallsigns)} {_playerCallsign} {call}");
            }

            foreach(string sample in awacsSamples)
            {
                AwacsRecordingControlList.Children.Add(
                    new RecordingControl(null, sample)
                );
            }
        }

        private void PopulateAtcSamples()
        {
            foreach (var call in Data.CaucasusAirfieldNames.Keys.SelectMany(GetRandomAtcCalls))
            {
                CaucasusRecordingControlList.Children.Add(new RecordingControl(Data.CaucasusAirfieldNames, call));
            }

            foreach (var call in Data.PersianGulfAirfieldNames.Keys.SelectMany(GetRandomAtcCalls))
            {
                PersianGulfRecordingControlList.Children.Add(new RecordingControl(Data.PersianGulfAirfieldNames, call));
            }

            foreach (var call in Data.SyriaAirfieldNames.Keys.SelectMany(GetRandomAtcCalls))
            {
                SyriaRecordingControlList.Children.Add(new RecordingControl(Data.SyriaAirfieldNames, call));
            }
        }

        private IEnumerable<string> GetRandomAtcCalls(string airfield)
        {
            var defaultCall =
                $"{airfield} {Random(Data.TaxiAirfieldControllers)} {_playerCallsign} {Random(Data.TaxiCalls)}";

            string[] atcCalls =
            {
                $"{airfield} {Random(Data.TaxiAirfieldControllers)} {_playerCallsign} holding short {Random(Data.Runways)}",
                $"{airfield} {Random(Data.TaxiAirfieldControllers)} {_playerCallsign} inbound",
                $"{airfield} {Random(Data.TaxiAirfieldControllers)} {_playerCallsign} request taxi to ramp",
                $"{airfield} {Random(Data.TaxiAirfieldControllers)} {_playerCallsign} requesting taxi to apron",
                $"{airfield} {Random(Data.TaxiAirfieldControllers)} {_playerCallsign} copy",
                $"{airfield} {Random(Data.TaxiAirfieldControllers)} {_playerCallsign} roger",
                $"{airfield} {Random(Data.TaxiAirfieldControllers)} {_playerCallsign} line up and wait {Random(Data.Runways)}",
                $"{airfield} {Random(Data.TaxiAirfieldControllers)} {_playerCallsign} crossing {Random(Data.Runways)}"
            };

            return atcCalls.ToList().OrderBy(arg => Guid.NewGuid()).Take(1).Append(defaultCall);
        }

        private IEnumerable<RecordingControl> AllRecordingControls()
        {
            var awacs = AwacsRecordingControlList.Children.Cast<RecordingControl>().ToList();
            var caucasus = CaucasusRecordingControlList.Children.Cast<RecordingControl>().ToList();
            var persian = PersianGulfRecordingControlList.Children.Cast<RecordingControl>().ToList();
            var syria = SyriaRecordingControlList.Children.Cast<RecordingControl>().ToList();
            return awacs.Concat(caucasus).Concat(persian).Concat(syria).ToList();
        }

        private static string Random(Array array)
        {
            return array.GetValue(Randomizer.Next(array.Length)).ToString();
        }

        private void SaveRecordings(object sender, RoutedEventArgs e)
        {
            var totalRecordings = AllRecordingControls().Count(control => control.RecordingAccepted);

            var message = "We are relying on you to submit accurate recordings, therefore make sure you have read, understood, and followed all the instructions\n\n" +
                          "Submitting inaccurate recordings makes the bot's voice recognition worse for everyone.\n\n" +
                          $"Save {totalRecordings} sample transmissions?";

            var caption = $"Read the following";
            var dialogResult = System.Windows.Forms.MessageBox.Show(message, caption,
                                         MessageBoxButtons.YesNo,
                                         MessageBoxIcon.Exclamation);

            if (dialogResult == System.Windows.Forms.DialogResult.No)
            {
                return;
            }

            var transcriptions = new List<string>();

            var tmpFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "OverlordBotTrainer");
            if (Directory.Exists(tmpFolder))
            {
                Directory.Delete(tmpFolder, true);
            }
            Directory.CreateDirectory(tmpFolder);

            foreach(RecordingControl control in AllRecordingControls())
            {
                if (control.RecordingAccepted != true) continue;
                var text = control.SampleSentence.Text;
                var audioData = control.RecordedAudio;
                audioData.Position = 0;

                var filename = $"{Guid.NewGuid()}.wav";
                transcriptions.Add($"{filename}\t{text}");

                var fullPath = Path.Combine(tmpFolder, filename);

                using (var fs = new FileStream(fullPath, FileMode.Create, FileAccess.Write))
                {
                    var buffer = new MemoryStream();
                    audioData.CopyTo(buffer);
                    fs.Write(buffer.GetBuffer(), 0, (int)buffer.Length);
                }
            }

            Encoding utf8WithoutBom = new UTF8Encoding(false);
            using (var transcriptionWriter = new StreamWriter(Path.Combine(tmpFolder, "Trans.txt"), false, utf8WithoutBom))
            {
                foreach (var s in transcriptions)
                {
                    transcriptionWriter.WriteLine(s);
                }
            }

            var zipFileName = $"OverlordBot-{DateTime.Now:yyyy-MM-dd}-{_playerCallsign.Replace(" ","-")}.zip";
            var zipFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, zipFileName);

            if (File.Exists(zipFilePath))
            {
                File.Delete(zipFilePath);
            }

            ZipFile.CreateFromDirectory(tmpFolder, zipFilePath);
            Directory.Delete(tmpFolder, true);

            var result = System.Windows.Forms.MessageBox.Show($"{zipFileName} Created\nUpload it to the OverlordBot Discord #voice-recordings channel\nClick OK to open the Folder containing the Zip File\nRead the README.txt file for the discord invite link if needed","Recordings Created", MessageBoxButtons.OK);

            if (result == System.Windows.Forms.DialogResult.OK)
            {
                Process.Start(AppDomain.CurrentDomain.BaseDirectory);
            }
        }
    }
}
